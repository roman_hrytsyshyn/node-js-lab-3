const dotenv = require('dotenv').config();
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const routes = require('./routes/index');

const errorMiddleware = require('./middleware/errorMiddleware');

const app = express();

app.use(cors());
app.use(express.json());

//router
app.use('/api', routes);

// Error
app.use(errorMiddleware);

// CONSTANTS
const PORT = process.env.PORT || 8080;
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_NAME = process.env.DB_NAME;

async function start() {
    try {
        await mongoose.connect(
            `mongodb+srv://${DB_USER}:${DB_PASSWORD}@cluster0.uu8ls2m.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`
        );
        app.listen(PORT, () => console.log(`Server started at port: ${PORT}`))
    } catch (err) {
        console.log(err);
    }
}

start();