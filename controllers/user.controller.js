const { User } = require('../models/User');
const { Truck } = require('../models/Truck');
const { Load } = require('../models/Load');
const ApiError = require('../error/ApiError');
const bcrypt = require('bcryptjs');

async function isUserOnLoad(_id, role) {
    if( role === 'SHIPPER') return false;


    const activeTruck = await Truck.findOne({assigned_to: _id});

    if( !activeTruck ) return false;
    return activeTruck.status === "OL";
}

class UserController {
    async getUser(req, res, next) {
        try {
            const user = await User.findById(req.user._id);

            if( !user ) return next(ApiError.badRequest('User doesn\'t exist'));

            res.status(200).json({
                _id: user._id,
                role: user.role,
                email: user.email,
                created_date: user.created_date
            })

        } catch (err) {
            return next(ApiError.internalError(err.message));
        }
    }
    async deleteUser(req, res, next) {
        try {
            const user = await User.findById(req.user._id);

            if(await isUserOnLoad(user._id, user.role)) return next(ApiError.badRequest('Can\'t change nothing while on load'));

            const isActiveLoads = await Load.findOne({ status: "ASSIGNED"});

            if( isActiveLoads ) return next(ApiError.badRequest('Can\'t delete account, while active loads'));

            if( !user ) return next(ApiError.badRequest('User doesn\'t exist'));

            await User.findByIdAndDelete(user._id);
            await Truck.deleteMany({ created_by: user._id});
            await Load.deleteMany({ created_by: user._id});

            res.status(200).json({message: 'Profile deleted successfully'})

        } catch (err) {
            return next(ApiError.internalError(err.message));
        }
    }
    async changePassword(req, res, next) {
        try {
            const user = await User.findById(req.user._id);

            if(await isUserOnLoad(user._id, user.role)) return next(ApiError.badRequest('Can\'t change nothing while on load'))

            if( !user ) return next(ApiError.badRequest('User doesn\'t exist'));

            const { oldPassword, newPassword} = req.body;

            if( !oldPassword || !newPassword ) return next(ApiError.badRequest('Not all parameters is provided'))

            const isPasswordCorrect = await bcrypt.compare(oldPassword, user.password );

            if( !isPasswordCorrect ) return next(ApiError.badRequest('Old password is incorrect'));

            const newHashedPassword = await bcrypt.hash(newPassword, 10);

            await User.findByIdAndUpdate(user._id, { password: newHashedPassword });

            res.status(200).json({message: 'Password changed successfully'});

        } catch (err) {
            return next(ApiError.internalError(err.message));
        }
    }
}

module.exports = new UserController();